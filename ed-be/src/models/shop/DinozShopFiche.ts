import { DinozRace } from '../index.js';

export interface DinozShopFiche {
	id: string;
	display: string;
	race: DinozRace;
}
