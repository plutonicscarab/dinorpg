export interface EpicReward {
	playerId: number;
	rewardId: number;
	name: string;
}
