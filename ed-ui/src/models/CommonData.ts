import { Dinoz } from './dinoz';

export interface CommonData {
	money: number;
	dinoz: Array<Dinoz>;
	dinozCount: number;
	id: number;
}
