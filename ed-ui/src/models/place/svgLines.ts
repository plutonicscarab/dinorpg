export interface svgLines {
	x1: number;
	y1: number;
	x2: number;
	y2: number;
	name: string;
}
