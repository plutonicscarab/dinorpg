import { DinozRace } from '@/models';

export interface DinozShop {
	id: number;
	display: string;
	race: DinozRace;
}
