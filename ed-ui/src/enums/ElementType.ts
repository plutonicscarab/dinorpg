export enum ElementType {
	fire = 1,
	wood = 2,
	water = 3,
	lightning = 4,
	air = 5,
	void = 6
}
