export enum Map {
	DINOLAND = 'zone_roydin',
	DINOWEST = 'zone_dnwest2',
	JUNGLE = 'zone_jungle',
	ILES = 'zone_ileatl',
	GTOUTCHAUD = 'zone_tchaud',
	STEPPE = 'zone_magnet',
	NIMBAO = 'zone_nimbao',
	ILEMONSTRE = 'zone_monisl'
}
