export enum PlaceIcon {
	CHURCH = 'church',
	CASTLE = 'castle',
	HOUSE = 'house',
	CAVERN = 'cavern',
	FOUNT = 'fount',
	DEFAULT = 'default',
	NORTH = 'north',
	SWIM = 'swim',
	WEST = 'west',
	EAST = 'east',
	SOUTH = 'south',
	MOUNTAIN = 'mountain',
	FOREST = 'forest',
	DOOR = 'door',
	WATER = 'water',
	RASCA = 'rasca',
	CLINIK = 'clinik'
}
