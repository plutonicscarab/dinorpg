export const epicList = {
	imgName: {
		1: 'perle',
		2: 'pteroz',
		3: 'hippo',
		4: 'rocky',
		5: 'quetzu',
		6: 'labowi',
		7: 'tour',
		8: 'vener',
		9: 'taurus',
		10: 'belius',
		11: 'scorp',
		12: 'msg',
		13: 'magnet',
		14: 'plume',
		15: 'kaura',
		16: 'demon',
		17: 'pmi',
		18: 'pda',
		19: 'fdjv1',
		20: 'dico1',
		21: 'dicarb',
		22: 'caush',
		23: 'fmedal',
		24: 'war1a',
		25: 'war1b',
		26: 'war2a',
		27: 'war2b',
		28: 'war3a',
		29: 'war3b',
		30: 'war4a',
		31: 'war4b',
		32: 'war5a',
		33: 'war5b',
		34: 'warfa',
		35: 'warfb',
		36: 'warga',
		37: 'wargb',
		38: 'warha',
		39: 'warhb',
		40: 'waria',
		41: 'warib',
		42: 'waric',
		43: 'warja',
		44: 'warjb',
		45: 'warjc',
		46: 'warka',
		47: 'warkb',
		48: 'warkc',
		49: 'warla',
		50: 'warlb',
		51: 'warlc',
		52: 'warma',
		53: 'warmb',
		54: 'warmc',
		55: 'warna',
		56: 'warnb',
		57: 'warnc',
		58: 'waroa',
		59: 'warob',
		60: 'waroc',
		61: 'warpa',
		62: 'warpb',
		63: 'warpc',
		64: 'warqa',
		65: 'warqb',
		66: 'warqc',
		67: 'warra',
		68: 'warrb',
		69: 'warrc',
		70: 'warsa',
		71: 'warsb',
		72: 'warsc',
		73: 'warta',
		74: 'wartb',
		75: 'wartc',
		76: 'warua',
		77: 'warub',
		78: 'waruc',
		79: 'warva',
		80: 'warvb',
		81: 'warvc',
		82: 'warwa',
		83: 'warwb',
		84: 'warwc',
		85: 'warxa',
		86: 'warxb',
		87: 'warxc',
		88: 'warya',
		89: 'waryb',
		90: 'waryc',
		91: 'warza',
		92: 'warzb',
		93: 'warzc',
		94: 'ch1x1a',
		95: 'ch1x1b',
		96: 'ch1x2a',
		97: 'ch1x2b',
		98: 'ch2x1a',
		99: 'ch2x2a',
		100: 'ch2x2b',
		101: 'ch2x3a',
		102: 'ch2x3b',
		103: 'ch3x1a',
		104: 'ch3x1b',
		105: 'ch3x2a',
		106: 'ch3x2b',
		107: 'ch3x3a',
		108: 'ch3x3b',
		109: 'ch3x4a',
		110: 'ch3x4b',
		111: 'ch4x1a',
		112: 'ch4x1b',
		113: 'ch4x2a',
		114: 'ch4x2b',
		115: 'ch4x3a',
		116: 'ch4x3b',
		117: 'ch4x4a',
		118: 'ch4x4b',
		119: 'ch5x1',
		120: 'ch5x2',
		121: 'ch5x3',
		122: 'ch5x4',
		123: 'chfx1',
		124: 'chfx2',
		125: 'chfx3',
		126: 'chfx4',
		127: 'chgx1a',
		128: 'chgx1b',
		129: 'chgx2a',
		130: 'chgx2b',
		131: 'chgx3a',
		132: 'chgx3b',
		133: 'chgx4a',
		134: 'chgx4b',
		135: 'chhx1a',
		136: 'chhx1b',
		137: 'chhx2a',
		138: 'chhx2b',
		139: 'chhx3a',
		140: 'chhx3b',
		141: 'chhx4a',
		142: 'chhx4b',
		143: 'chix1a',
		144: 'chix1b',
		145: 'chix2a',
		146: 'chix2b',
		147: 'chix3a',
		148: 'chix3b',
		149: 'chix4b',
		150: 'chix4a',
		151: 'chjx1a',
		152: 'chjx1b',
		153: 'chjx2a',
		154: 'chjx2b',
		155: 'chjx3a',
		156: 'chjx3b',
		157: 'chkx1b',
		158: 'chkx1a',
		159: 'chkx2b',
		160: 'chkx2a',
		161: 'chkx3b',
		162: 'chkx3a',
		163: 'chlx1b',
		164: 'chlx1a',
		165: 'chlx2b',
		166: 'chlx2a',
		167: 'chlx3b',
		168: 'chlx3a',
		169: 'chmx1b',
		170: 'chmx1a',
		171: 'chmx2b',
		172: 'chmx2a',
		173: 'chmx3b',
		174: 'chmx3a',
		175: 'chnx1b',
		176: 'chnx1a',
		177: 'chnx2b',
		178: 'chnx2a',
		179: 'chnx3b',
		180: 'chnx3a',
		181: 'chox1a',
		182: 'chox1b',
		183: 'chox2a',
		184: 'chox2b',
		185: 'chox3a',
		186: 'chox3b',
		187: 'chpx1a',
		188: 'chpx1b',
		189: 'chpx2a',
		190: 'chpx2b',
		191: 'chpx3a',
		192: 'chpx3b',
		193: 'cinec1',
		194: 'cinec2',
		195: 'cinec3',
		196: 'cinec4',
		197: 'hallow',
		198: 'tid1',
		199: 'tid2',
		200: 'gazeli',
		201: 'hunt1',
		202: 'hunint',
		203: 'xhunt3',
		204: 'xhunt2',
		205: 'xhunt1',
		206: 'cinema',
		207: 'cinem2',
		208: 'eugene',
		209: 'litter',
		210: 'clover',
		999: 'import'
	},
	id: {
		perle: 1,
		pteroz: 2,
		hippo: 3,
		rocky: 4,
		quetzu: 5,
		labowi: 6,
		tour: 7,
		vener: 8,
		taurus: 9,
		belius: 10,
		scorp: 11,
		msg: 12,
		magnet: 13,
		plume: 14,
		kaura: 15,
		demon: 16,
		pmi: 17,
		pda: 18,
		fdjv1: 19,
		dico1: 20,
		dicarb: 21,
		caush: 22,
		fmedal: 23,
		war1a: 24,
		war1b: 25,
		war2a: 26,
		war2b: 27,
		war3a: 28,
		war3b: 29,
		war4a: 30,
		war4b: 31,
		war5a: 32,
		war5b: 33,
		warfa: 34,
		warfb: 35,
		warga: 36,
		wargb: 37,
		warha: 38,
		warhb: 39,
		waria: 40,
		warib: 41,
		waric: 42,
		warja: 43,
		warjb: 44,
		warjc: 45,
		warka: 46,
		warkb: 47,
		warkc: 48,
		warla: 49,
		warlb: 50,
		warlc: 51,
		warma: 52,
		warmb: 53,
		warmc: 54,
		warna: 55,
		warnb: 56,
		warnc: 57,
		waroa: 58,
		warob: 59,
		waroc: 60,
		warpa: 61,
		warpb: 62,
		warpc: 63,
		warqa: 64,
		warqb: 65,
		warqc: 66,
		warra: 67,
		warrb: 68,
		warrc: 69,
		warsa: 70,
		warsb: 71,
		warsc: 72,
		warta: 73,
		wartb: 74,
		wartc: 75,
		warua: 76,
		warub: 77,
		waruc: 78,
		warva: 79,
		warvb: 80,
		warvc: 81,
		warwa: 82,
		warwb: 83,
		warwc: 84,
		warxa: 85,
		warxb: 86,
		warxc: 87,
		warya: 88,
		waryb: 89,
		waryc: 90,
		warza: 91,
		warzb: 92,
		warzc: 93,
		ch1x1a: 94,
		ch1x1b: 95,
		ch1x2a: 96,
		ch1x2b: 97,
		ch2x1a: 98,
		ch2x2a: 99,
		ch2x2b: 100,
		ch2x3a: 101,
		ch2x3b: 102,
		ch3x1a: 103,
		ch3x1b: 104,
		ch3x2a: 105,
		ch3x2b: 106,
		ch3x3a: 107,
		ch3x3b: 108,
		ch3x4a: 109,
		ch3x4b: 110,
		ch4x1a: 111,
		ch4x1b: 112,
		ch4x2a: 113,
		ch4x2b: 114,
		ch4x3a: 115,
		ch4x3b: 116,
		ch4x4a: 117,
		ch4x4b: 118,
		ch5x1: 119,
		ch5x2: 120,
		ch5x3: 121,
		ch5x4: 122,
		chfx1: 123,
		chfx2: 124,
		chfx3: 125,
		chfx4: 126,
		chgx1a: 127,
		chgx1b: 128,
		chgx2a: 129,
		chgx2b: 130,
		chgx3a: 131,
		chgx3b: 132,
		chgx4a: 133,
		chgx4b: 134,
		chhx1a: 135,
		chhx1b: 136,
		chhx2a: 137,
		chhx2b: 138,
		chhx3a: 139,
		chhx3b: 140,
		chhx4a: 141,
		chhx4b: 142,
		chix1a: 143,
		chix1b: 144,
		chix2a: 145,
		chix2b: 146,
		chix3a: 147,
		chix3b: 148,
		chix4b: 149,
		chix4a: 150,
		chjx1a: 151,
		chjx1b: 152,
		chjx2a: 153,
		chjx2b: 154,
		chjx3a: 155,
		chjx3b: 156,
		chkx1b: 157,
		chkx1a: 158,
		chkx2b: 159,
		chkx2a: 160,
		chkx3b: 161,
		chkx3a: 162,
		chlx1b: 163,
		chlx1a: 164,
		chlx2b: 165,
		chlx2a: 166,
		chlx3b: 167,
		chlx3a: 168,
		chmx1b: 169,
		chmx1a: 170,
		chmx2b: 171,
		chmx2a: 172,
		chmx3b: 173,
		chmx3a: 174,
		chnx1b: 175,
		chnx1a: 176,
		chnx2b: 177,
		chnx2a: 178,
		chnx3b: 179,
		chnx3a: 180,
		chox1a: 181,
		chox1b: 182,
		chox2a: 183,
		chox2b: 184,
		chox3a: 185,
		chox3b: 186,
		chpx1a: 187,
		chpx1b: 188,
		chpx2a: 189,
		chpx2b: 190,
		chpx3a: 191,
		chpx3b: 192,
		cinec1: 193,
		cinec2: 194,
		cinec3: 195,
		cinec4: 196,
		hallow: 197,
		tid1: 198,
		tid2: 199,
		gazeli: 200,
		hunt1: 201,
		hunint: 202,
		xhunt3: 203,
		xhunt2: 204,
		xhunt1: 205,
		cinema: 206,
		cinem2: 207,
		eugene: 208,
		litter: 209,
		clover: 210,
		import: 999
	}
};
