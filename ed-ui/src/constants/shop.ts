export const shopNameList: Array<string> = [
	'unknown',
	'flying',
	'forge',
	'magic',
	'cursed', // picture missing
	'fruity',
	'razad',
	'souk',
	'neerhel',
	'barbarian',
	'secret',
	'elit', // original game has no picture
	'chens' // picture missing
];
